const canvas = document.getElementById("canvas");
const ctx2d = canvas.getContext("2d");
let img = null;

const observer = new ResizeObserver((e) => {
  const { width, height } = e[0].contentRect;
  canvas.width = width;
  canvas.height = height;

  initCtx(ctx2d);
  drawAxis(ctx2d);
  draw(ctx2d);
  img = ctx2d.getImageData(0, 0, canvas.width, canvas.height);
});
observer.observe(document.body);
// observer.unobserve(document.body);
// observer.disconnect();

const initCtx = (ctx) => {
  if (ctx) {
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.scale(1, -1);
  }
};

const drawAxis = (ctx) => {
  if (ctx) {
    ctx.beginPath();
    ctx.strokeStyle = "#FfFfFf80";
    ctx.lineWidth = 1;
    ctx.moveTo(0, 0);
    ctx.lineTo(canvas.width / 2 - 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, canvas.height / 2 - 10);
    ctx.moveTo(0, 0);
    ctx.lineTo(-canvas.width / 2 + 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, -canvas.height / 2 + 10);
    ctx.stroke();
  }
};

const draw = (ctx) => {
  if (ctx) {
  }
};

const test = (ctx, x, y) => {
  const p = (x + canvas.width * y) * 4;
  img.data[p] = 255;
  // img.data[p + 1] = 255;
  // img.data[p + 2] = 255;
  img.data[p + 3] = 255;
  ctx.putImageData(img, 0, 0);
};

const onMouseMove = (e) => {
  // window.console.log(e);
  test(ctx2d, e.offsetX, e.offsetY);
};
