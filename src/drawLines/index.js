const canvas = document.getElementById("canvas");
const ctx2d = canvas.getContext("2d");

const observer = new ResizeObserver((e) => {
  const { width, height } = e[0].contentRect;
  canvas.width = width;
  canvas.height = height;

  initCtx(ctx2d);
  drawAxis(ctx2d);
  draw(ctx2d);
});
observer.observe(document.body);
// observer.unobserve(document.body);
// observer.disconnect();

const initCtx = (ctx) => {
  if (ctx) {
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.scale(1, -1);
  }
};

const drawAxis = (ctx) => {
  if (ctx) {
    ctx.beginPath();
    ctx.strokeStyle = "#FfFfFf80";
    ctx.lineWidth = 1;
    ctx.moveTo(0, 0);
    ctx.lineTo(canvas.width / 2 - 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, canvas.height / 2 - 10);
    ctx.moveTo(0, 0);
    ctx.lineTo(-canvas.width / 2 + 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, -canvas.height / 2 + 10);
    ctx.stroke();
  }
};

const draw = (ctx) => {
  if (ctx) {
  }
};

let lastPoint = null;
const onMouseMove = (e) => {
  const p = { x: e.offsetX, y: e.offsetY };
  if (lastPoint) {
    if (ctx2d) {
      ctx2d.beginPath();
      ctx2d.strokeStyle = "#FfFfFf80";
      ctx2d.lineWidth = 1;
      ctx2d.moveTo(
        lastPoint.x - canvas.width / 2,
        -lastPoint.y + canvas.height / 2
      );
      ctx2d.lineTo(p.x - canvas.width / 2, -p.y + canvas.height / 2);
      ctx2d.stroke();
    }
  }
  lastPoint = p;
};
