const canvas = document.getElementById("canvas");
const ctx2d = canvas.getContext("2d");

const observer = new ResizeObserver((e) => {
  const { width, height } = e[0].contentRect;
  canvas.width = width;
  canvas.height = height;

  initCtx(ctx2d);
  drawAxis(ctx2d);
  draw(ctx2d);
});
observer.observe(document.body);
// observer.unobserve(document.body);
// observer.disconnect();

const initCtx = (ctx) => {
  if (ctx) {
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.scale(1, -1);
  }
};

const drawAxis = (ctx) => {
  if (ctx) {
    ctx.beginPath();
    ctx.strokeStyle = "#FfFfFf80";
    ctx.lineWidth = 1;
    ctx.moveTo(0, 0);
    ctx.lineTo(canvas.width / 2 - 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, canvas.height / 2 - 10);
    ctx.moveTo(0, 0);
    ctx.lineTo(-canvas.width / 2 + 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, -canvas.height / 2 + 10);
    ctx.stroke();
  }
};

const draw = (ctx) => {
  function func(x) {
    return x + 20;
  }
  const right = canvas.width / 2;
  const RW = 100;
  if (ctx) {
    ctx.beginPath();
    ctx.strokeStyle = "#Ff000080";
    ctx.lineWidth = 1;
    ctx.moveTo(-20, func(-20));
    for (let i = -20; i < 20; i += 1) {
      const x1 = (i / RW) * right;
      ctx.lineTo(x1, func(x1));
    }
    ctx.stroke();
  }
};
