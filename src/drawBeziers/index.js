const canvas = document.getElementById("canvas");
const ctx2d = canvas.getContext("2d");

const observer = new ResizeObserver((e) => {
  const { width, height } = e[0].contentRect;
  canvas.width = width;
  canvas.height = height;

  initCtx(ctx2d);
  drawAxis(ctx2d);
  draw(ctx2d);
});
observer.observe(document.body);
// observer.unobserve(document.body);
// observer.disconnect();

const initCtx = (ctx) => {
  if (ctx) {
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.scale(1, -1);
  }
};

const drawAxis = (ctx) => {
  if (ctx) {
    ctx.beginPath();
    ctx.strokeStyle = "#FfFfFf80";
    ctx.lineWidth = 1;
    ctx.moveTo(0, 0);
    ctx.lineTo(canvas.width / 2 - 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, canvas.height / 2 - 10);
    ctx.moveTo(0, 0);
    ctx.lineTo(-canvas.width / 2 + 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, -canvas.height / 2 + 10);
    ctx.stroke();
  }
};

const draw = (ctx) => {
  if (ctx) {
    drawSolve(ctx, [10, 10, 50, 50, 90, 50, 130, 10]);

    const pp = [
      { X: 10, Y: 20 },
      { X: 50, Y: 60 },
      { X: 90, Y: 60 },
      { X: 130, Y: 20 },
    ];
    // TODO test getBeziers
  }
};

function drawSolve(ctx, data, k = null, color = null) {
  if (k === null) k = 1;
  var size = data.length;
  var last = size - 4;
  ctx.strokeStyle = color || "#Fff0f0";
  ctx.beginPath();
  ctx.moveTo(data[0], data[1]);
  for (var i = 0; i < size - 2; i += 2) {
    var x0 = i ? data[i - 2] : data[0];
    var y0 = i ? data[i - 1] : data[1];
    var x1 = data[i + 0];
    var y1 = data[i + 1];
    var x2 = data[i + 2];
    var y2 = data[i + 3];
    var x3 = i !== last ? data[i + 4] : x2;
    var y3 = i !== last ? data[i + 5] : y2;
    var cp1x = x1 + ((x2 - x0) / 6) * k;
    var cp1y = y1 + ((y2 - y0) / 6) * k;
    var cp2x = x2 - ((x3 - x1) / 6) * k;
    var cp2y = y2 - ((y3 - y1) / 6) * k;
    ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x2, y2);
  }
  ctx.stroke();
}

function getBeziers(data, k = null) {
  const beziers = [];
  if (k == null) k = 1;
  var size = data.Length;
  var last = size - 2;
  beziers.push(data[0]);
  for (var i = 0; i < size - 1; i += 1) {
    var p0 = i > 0 ? data[i - 1] : data[0];
    var p1 = data[i];
    var p2 = data[i + 1];
    var p3 = i != last ? data[i + 2] : p2;

    var cp1 = {
      X: p1.X + ((p2.X - p0.X) / 6) * k,
      Y: p1.Y + ((p2.Y - p0.Y) / 6) * k,
    };
    var cp2 = {
      X: p2.X - ((p3.X - p1.X) / 6) * k,
      Y: p2.Y - ((p3.Y - p1.Y) / 6) * k,
    };

    beziers.push(cp1);
    beziers.push(cp2);
    beziers.push(p2);
  }
  return beziers;
}
