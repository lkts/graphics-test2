const canvas = document.getElementById("canvas");
const ctx2d = canvas.getContext("2d");
let drawable = false;

const observer = new ResizeObserver((e) => {
  const { width, height } = e[0].contentRect;
  canvas.width = width;
  canvas.height = height;

  initCtx(ctx2d);
  // drawAxis(ctx2d);
  // draw(ctx2d);
});
observer.observe(document.body);
// observer.unobserve(document.body);
// observer.disconnect();

const initCtx = (ctx) => {
  if (ctx) {
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.scale(1, -1);
    drawable = true;
  }
};

const drawAxis = (ctx) => {
  if (ctx) {
    ctx.beginPath();
    ctx.strokeStyle = "#FfFfFf20";
    ctx.lineWidth = 1;
    ctx.moveTo(0, 0);
    ctx.lineTo(canvas.width / 2 - 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, canvas.height / 2 - 10);
    ctx.moveTo(0, 0);
    ctx.lineTo(-canvas.width / 2 + 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, -canvas.height / 2 + 10);
    ctx.stroke();
  }
};

const draw = (ctx, theta = 0) => {
  if (ctx) {
    ctx.clearRect(
      -canvas.width / 2,
      -canvas.height / 2,
      canvas.width,
      canvas.height
    );
    ctx.strokeStyle = "#Ffffff";

    ctx.beginPath();
    // ctx.ellipse(0, 0, 50, 50, 0, 0, 2 * Math.PI);
    for (let i = 0; i < 10000; i++) {
      const a1 = i * 0.1 + theta;
      const a2 = (i + 1) * 0.1 + theta;
      ctx.arc(0, 0, 0 + i * 0.2, a1, a2);
    }
    ctx.stroke();
  }
};

let i = 0;
(function run() {
  if (drawable) {
    draw(ctx2d, i);
    i += 0.5;
    drawAxis(ctx2d);
  }
  return requestAnimationFrame(run);
})();
