const canvas = document.getElementById("canvas");
const ctx2d = canvas.getContext("2d");
let img = null;

const observer = new ResizeObserver((e) => {
  const { width, height } = e[0].contentRect;
  canvas.width = width;
  canvas.height = height;

  initCtx(ctx2d);
  drawAxis(ctx2d);
  draw(ctx2d);
  img = ctx2d.getImageData(0, 0, canvas.width, canvas.height);
});
observer.observe(document.body);
// observer.unobserve(document.body);
// observer.disconnect();

const initCtx = (ctx) => {
  if (ctx) {
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.scale(1, -1);
  }
};

const drawAxis = (ctx) => {
  if (ctx) {
    ctx.beginPath();
    ctx.strokeStyle = "#FfFfFf80";
    ctx.lineWidth = 1;
    ctx.moveTo(0, 0);
    ctx.lineTo(canvas.width / 2 - 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, canvas.height / 2 - 10);
    ctx.moveTo(0, 0);
    ctx.lineTo(-canvas.width / 2 + 10, 0);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, -canvas.height / 2 + 10);
    ctx.stroke();
  }
};

const draw = (ctx) => {
  if (ctx) {
  }
};

const test = (ctx, x, y) => {
  const p = (x + canvas.width * y) * 4;
  img.data[p] = 255;
  // img.data[p + 1] = 255;
  // img.data[p + 2] = 255;
  img.data[p + 3] = 255;
  ctx.putImageData(img, 0, 0);
};

const onMouseMove = (e) => {
  // window.console.log(e);
  // test(ctx2d, e.offsetX, e.offsetY);
};

let P0 = null;
const onMouseDown = (e) => {
  // window.console.log(e);
  P0 = { x: e.offsetX, y: e.offsetY };
};

const onMouseUp = (e) => {
  // window.console.log(e);
  let P1 = { x: e.offsetX, y: e.offsetY };
  let p, t;
  if (Math.abs(P0.x - P1.x) < 1e-6) {
    if (P0.y > P1.y) {
      t = P0;
      P0 = P1;
      P1 = t;
    }
    for (p = P0; p.y < P1.y; p.y++) {
      const pp = (p.x + canvas.width * p.y) * 4;
      img.data[pp] = 255;
      // img.data[p + 1] = 255;
      // img.data[p + 2] = 255;
      img.data[pp + 3] = 255;
    }
  } else {
    let k, d;
    k = (P1.y - P0.y) / (P1.x - P0.x);
    if (k > 1.0) {
      if (P0.y > P1.y) {
        t = P0;
        P0 = P1;
        P1 = t;
      }
      d = 1 - 0.5 * k;
      for (p = P0; p.y < P1.y; p.y++) {
        const pp = (p.x + canvas.width * p.y) * 4;
        img.data[pp] = 255;
        // img.data[p + 1] = 255;
        // img.data[p + 2] = 255;
        img.data[pp + 3] = 255;
        if (d >= 0) {
          p.x++;
          d += 1 - k;
        } else d += 1;
      }
    }
    if (0.0 <= k && k <= 1.0) {
      if (P0.x > P1.x) {
        t = P0;
        P0 = P1;
        P1 = t;
      }
      d = 0.5 - k;
      for (p = P0; p.x < P1.x; p.x++) {
        const pp = (p.x + canvas.width * p.y) * 4;
        img.data[pp] = 255;
        // img.data[p + 1] = 255;
        // img.data[p + 2] = 255;
        img.data[pp + 3] = 255;
        if (d < 0) {
          p.y++;
          d += 1 - k;
        } else d -= k;
      }
    }
    if (k >= -1.0 && k < 0.0) {
      if (P0.x > P1.x) {
        t = P0;
        P0 = P1;
        P1 = t;
      }
      d = -0.5 - k;
      for (p = P0; p.x < P1.x; p.x++) {
        const pp = (p.x + canvas.width * p.y) * 4;
        // window.console.log({ pp, ...p });
        img.data[pp] = 255;
        // img.data[pp + 1] = 255;
        // img.data[pp + 2] = 255;
        img.data[pp + 3] = 255;
        if (d > 0) {
          p.y--;
          d -= 1 + k;
        } else d -= k;
      }
    }
    if (k < -1.0) {
      if (P0.y < P1.y) {
        t = P0;
        P0 = P1;
        P1 = t;
      }
      d = -1 - 0.5 * k;
      for (p = P0; p.y > P1.y; p.y--) {
        const pp = (p.x + canvas.width * p.y) * 4;
        img.data[pp] = 255;
        // img.data[p + 1] = 255;
        // img.data[p + 2] = 255;
        img.data[pp + 3] = 255;
        if (d < 0) {
          p.x++;
          d -= 1 + k;
        } else d -= 1;
      }
    }
  }
  P0 = P1;
  ctx2d.putImageData(img, 0, 0);
};
